from django.db import models
from imagekit.models import ImageSpecField
from imagekit.processors import ResizeToFill
# listings/models.py

class Band(models.Model):
     
    name = models.fields.CharField(max_length=100)



class Article(models.Model):

    name = models.fields.CharField(max_length=100)
    price = models.fields.IntegerField()
    details = models.fields.CharField(max_length=100)
    image = models.ImageField(upload_to='img_desc/')

    def __str__(self):
        return f'{self.name}'