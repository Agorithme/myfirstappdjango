# ~/projects/django-web-app/merchex/listings/views.py

from http.client import OK
from json import JSONEncoder
from django.http import HttpResponse
from django.shortcuts import render
from listings.models import Article


def hello(request):
    Articles = Article.objects.all()
    return render(request, 'listings/hello.html',
                  context={"Articles" : Articles})

def about(request):
    return HttpResponse('<h1>À propos</h1> <p>Nous adorons merch !</p>')

def details(request, id):
    article = Article.objects.get(id=id)
    return render(request, 'listings/details.html', {'article':article})